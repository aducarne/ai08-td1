package com.ai08.td1;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.ai08.td1.databinding.ActivityMainBinding;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private final Currency INIT_FIRST_CURRENCY = new Currency("EUR", BigDecimal.ONE);
    private final Currency INIT_SECOND_CURRENCY = new Currency("USD", BigDecimal.valueOf(1.1));
    private EditText textCurrencyLeft;
    private EditText textCurrencyRight;
    private Spinner spnCurrencyLeft;
    private Spinner spnCurrencyRight;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initComponents();
        initBindings();
    }

    private void initComponents() {
        textCurrencyLeft = findViewById(R.id.text_currency_left);
        textCurrencyRight = findViewById(R.id.text_currency_right);
        spnCurrencyLeft = findViewById(R.id.spn_currency_left);
        spnCurrencyRight = findViewById(R.id.spn_currency_right);

        ArrayAdapter<Currency> currencyAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, Currency.getAll());
        spnCurrencyLeft.setAdapter(currencyAdapter);
        spnCurrencyRight.setAdapter(currencyAdapter);
        textCurrencyLeft.requestFocus();
    }

    private void initBindings() {
        binding.setCurrentCurrencyLeft(INIT_FIRST_CURRENCY);
        binding.setCurrentCurrencyRight(INIT_SECOND_CURRENCY);

        spnCurrencyLeft.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Currency currency = (Currency) adapterView.getSelectedItem();
                binding.setCurrentCurrencyLeft(currency);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spnCurrencyRight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Currency currency = (Currency) adapterView.getSelectedItem();
                binding.setCurrentCurrencyRight(currency);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spnCurrencyRight.setSelection(1);
    }


    public void convertFromLeft(View v) {
        try {
            Double amount = Double.parseDouble(textCurrencyLeft.getText().toString().replaceAll(",", "."));
            String formattedStr = convertCurrency(amount, binding.getCurrentCurrencyRight(), binding.getCurrentCurrencyLeft());
            textCurrencyRight.setText(formattedStr);
        } catch (NumberFormatException | NullPointerException ignored) {
            textCurrencyRight.setText("");
        }
    }

    public void convertFromRight(View v) {
        try {
            Double amount = Double.parseDouble(textCurrencyRight.getText().toString().replaceAll(",", "."));
            String formattedStr = convertCurrency(amount, binding.getCurrentCurrencyLeft(), binding.getCurrentCurrencyRight());
            textCurrencyLeft.setText(formattedStr);
        } catch (NumberFormatException | NullPointerException e) {
            textCurrencyLeft.setText("");
        }
    }

    private String convertCurrency(Double amount, Currency currencyFrom, Currency currencyTo) {
        BigDecimal conversion = currencyFrom.getRateToEUR()
                .divide(currencyTo.getRateToEUR(), 12, RoundingMode.HALF_EVEN)
                .multiply(BigDecimal.valueOf(amount));

        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(conversion);
    }
}
