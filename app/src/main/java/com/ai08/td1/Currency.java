package com.ai08.td1;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class Currency {
    private String name;
    private BigDecimal rateToEUR;

    Currency(String name, BigDecimal rateToEUR) {
        this.name = name;
        this.rateToEUR = rateToEUR;
    }

    static List<Currency> getAll() {
        return Arrays.asList(new Currency("EUR", BigDecimal.ONE),
                new Currency("USD", BigDecimal.valueOf(1.10456)),
                new Currency("GBP", BigDecimal.valueOf(.8616479)),
                new Currency("CHF", BigDecimal.valueOf(1.0996491)),
                new Currency("JPY", BigDecimal.valueOf(120.7256927)),
                new Currency("CNY", BigDecimal.valueOf(7.7157349)),
                new Currency("INR", BigDecimal.valueOf(78.624))
        );
    }

    public String getName() {
        return name;
    }

    BigDecimal getRateToEUR() {
        return rateToEUR;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
